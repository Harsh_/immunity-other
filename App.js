import React, { Component } from 'react';
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack'
import {LoginScreen} from './Screens/LoginScreen/LoginScreen.js'
import {SplashScreen} from './Screens/SplashScreen/SplashScreen.js'
import {TermsScreen} from './Screens/TermsAndConditions/TermsScreen.js'
import {OnboardScreen} from './Screens/OnboardScreen/OnboardScreen.js'






const MyStackNavigator = createStackNavigator({

  SplashScreen:{
    screen: SplashScreen,
    navigationOptions: {
      gesturesEnabled: false,
  },
  },
  LoginScreen:{
    screen:LoginScreen,
    navigationOptions: {
      gesturesEnabled: false,
  },
  },
  TermsScreen:{
    screen:TermsScreen,
    navigationOptions: {
      gesturesEnabled: false,
  },
  },
  OnboardScreen:{
    screen:OnboardScreen,
    navigationOptions: {
      gesturesEnabled: false,
  },
  },
},
  {
    initialRouteName: 'SplashScreen',
    drawerWidth: 300,
    headerMode: 'none'
  }
);


const MyApp = createAppContainer(MyStackNavigator);


export default class App extends React.Component {
  render() {

    return <MyApp />;

  }
}
