import React, { Component, createRef } from 'react';
import { StyleSheet, BackHandler, SafeAreaView, FlatList, Alert, AsyncStorage, ScrollView, Image, View, Text, TextInput, TouchableOpacity, ImageBackground, KeyboardAvoidingView } from 'react-native';
import { styles } from './LoginStyle.js';
import * as Permissions from 'expo-permissions'
import Constants from 'expo-constants';
import { withNavigation, StackActions, NavigationActions } from 'react-navigation';
import * as Font from 'expo-font';
import Toast, { DURATION } from 'react-native-easy-toast'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';




export class LoginScreen extends Component {

    constructor(props) {

        super(props);

        this.state = { isLoading: true, userEmail: "", accessCode: "" };

        this.flist = createRef()

        console.log("construct called");

    }

    componentDidMount() {

        BackHandler.addEventListener('hardwareBackPress', () => this.handleBackButton());

    }

    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', () => this.handleBackButton());

    }



    handleBackButton = async () => {

        this.props.navigation.goBack();
    }

    getItemLayout = (data, index) => (
        { length: 50, offset: 50 * index, index }
    )


    async componentWillMount() {


        await Font.loadAsync({
            // Load a font `Montserrat` from a static resource

            'Futura': require('../../assets/Fonts/futura.ttf'),
            'Mont': require('../../assets/Fonts/futura.ttf'),
            'Bold': require('../../assets/Fonts/futura.ttf'),
            'Heavy': require('../../assets/Fonts/futura.ttf')
        });

        this.GenerateViews();


    }

    validateEmail = (email) => {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


    PreSignIn = () => {

        this.props.navigation.navigate("OTPScreen");

    }


    GenerateViews = () => {

        var viewArr = []

        var view1 = <TextInput style={{ height: 50, borderRadius: 40, minWidth: 290, marginTop: 20, backgroundColor: '#dadada', textAlign: 'center' }}
            placeholder={"Enter Email"}
            onChangeText={(userEmail) => this.setState({ userEmail })}
        />

        var view2 = <Text style={{ color: 'red', marginTop: 20, fontSize: 14, textAlign: 'left', fontFamily: 'Futura' }}>OR</Text>

        var view3 = <TextInput
            style={{ height: 50, borderRadius: 40, minWidth: 290, marginTop: 20, backgroundColor: '#dadada', textAlign: 'center' }}
            placeholder={"Enter Access Code"}
            onChangeText={(accessCode) => this.setState({ accessCode })}
        />

        var view4 = <TouchableOpacity style={styles.loginBtn2} onPress={() => this.PreSignIn()}>
            <Text style={{ color: 'white', fontSize: 14, textAlign: 'center', fontFamily: 'Futura' }}>Done</Text>
        </TouchableOpacity>


        viewArr.push(view1);
        viewArr.push(view2);
        viewArr.push(view3);
        viewArr.push(view4);


        this.setState({ views: viewArr });

        this.setState({ isLoading: false });


    }

    PreSignIn = () => {


        if (this.state.userEmail.length > 0 && this.state.accessCode > 0) {

            this.refs.toast.show('Please input either email or access code, not both!', 2500);

            return;

        }
        else if (this.state.userEmail == "" && this.state.accessCode == "") {

            this.refs.toast.show('Please input either email or access code!', 2500);

            return;
        }
        else if (this.state.userEmail.length > 0) {

            var viewArr = this.state.views;

            var view5 = <View>
                <Text style={{ color: 'red', marginTop: 20, fontSize: 14, textAlign: 'left', fontFamily: 'Futura' }}>Please Enter the OTP sent to your email address.</Text>
                <TextInput
                    style={{ height: 50, borderRadius: 40, minWidth: 290, marginTop: 20, backgroundColor: '#dadada', textAlign: 'center' }}
                    placeholder={"Enter Email OTP"}
                    onChangeText={(emailOTP) => this.setState({ emailOTP })}
                />
                <TouchableOpacity style={styles.loginBtn} onPress={() => this.EmailSignIn()}>
                    <Text style={{ color: 'white', fontSize: 14, textAlign: 'center', fontFamily: 'Futura' }}>Done</Text>
                </TouchableOpacity>

            </View>

            viewArr.push(view5);

            this.setState({ views: viewArr });

            this.flist.current.scrollToIndex({ animated: true, index: 4 });

            //this.flist.current.scrollToOffset({offset:5000, animated: true});



        }
        else if (this.state.accessCode.length > 0) {

            var viewArr = this.state.views;

            var view5 = <View>
                <Text style={{ color: 'red', marginTop: 20, fontSize: 14, textAlign: 'left', fontFamily: 'Futura' }}>Please enter your mobile number.</Text>
                <TextInput
                    style={{ height: 50, borderRadius: 40, minWidth: 290, marginTop: 20, backgroundColor: '#dadada', textAlign: 'center' }}
                    placeholder={"Enter Mobile Number"}
                    onChangeText={(mobileNum) => this.setState({ mobileNum })}
                />
                <TouchableOpacity style={styles.loginBtn} onPress={() => this.MobileSignIn()}>
                    <Text style={{ color: 'white', fontSize: 14, textAlign: 'center', fontFamily: 'Futura' }}>Done</Text>
                </TouchableOpacity>

            </View>


            viewArr.push(view5);

            this.setState({ views: viewArr });

            this.flist.current.scrollToIndex({ animated: true, index: 4 });

            this.flist.current.scrollToOffset({ offset: 7000, animated: true });




        }

    }


    EmailSignIn = () => {


        var viewArr = []

        var view5 = <View>
            <Text style={{ color: 'red', marginTop: 20, fontSize: 14, textAlign: 'left', fontFamily: 'Futura' }}>Please enter your mobile number.</Text>
            <TextInput
                style={{ height: 50, borderRadius: 40, minWidth: 290, marginTop: 20, backgroundColor: '#dadada', textAlign: 'center' }}
                placeholder={"Enter Mobile Number"}
                onChangeText={(mobileNum) => this.setState({ mobileNum })}
            />
            <TouchableOpacity style={styles.loginBtn} onPress={() => this.MobileSignIn()}>
                <Text style={{ color: 'white', fontSize: 14, textAlign: 'center', fontFamily: 'Futura' }}>Done</Text>
            </TouchableOpacity>

        </View>


        viewArr.push(view5);

        this.setState({ views: viewArr });

        this.flatListRef.scrollToIndex({ animated: true, index: 0 });


    }


    MobileSignIn = () => {

        var viewArr = []

        var view5 = <View>
            <Text style={{ color: 'blue', marginTop: 20, fontSize: 14, textAlign: 'left', fontFamily: 'Futura' }}>Please enter the OTP sent to your mobile number.</Text>
            <TextInput
                style={{ height: 50, borderRadius: 40, minWidth: 290, marginTop: 20, backgroundColor: '#dadada', textAlign: 'center' }}
                placeholder={"Enter Mobile Number"}
                onChangeText={(mobileNum) => this.setState({ mobileNum })}
            />
            <TouchableOpacity style={styles.loginBtn} onPress={() => this.GoToTerms()}>
                <Text style={{ color: 'white', fontSize: 14, textAlign: 'center', fontFamily: 'Futura' }}>Done</Text>
            </TouchableOpacity>

        </View>


        viewArr.push(view5);

        this.setState({ views: viewArr });

        this.flatListRef.scrollToIndex({ animated: true, index: 0 });



    }


    GoToTerms = () => {

        this.props.navigation.navigate("TermsScreen");
    }


    RenderView = (item) => {

        return (
            item
        );
    }




    render() {

        if (this.state.isLoading === false) {

            return (
                <KeyboardAwareScrollView enableOnAndroid={true}	showsVerticalScrollIndicator={false}>

                <SafeAreaView style={styles.container}>

                    <View contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>

                        <View style={styles.sectionTop}>


                            <Image
                                style={{ height: 55, resizeMode: 'contain', marginTop: 20 }}
                                source={require('../../assets/tlogo.png')}
                            />
                        </View>

                        {/* <KeyboardAvoidingView style={styles.sectionBottom} behavior='padding' keyboardVerticalOffset={10}> */}

                            <View style={{ justifyContent: 'flex-end', alignItems: 'flex-start', flex: 1, marginTop: 200 }}>

                                <Text style={{ color: 'red', marginTop: 20, fontSize: 14, textAlign: 'left', fontFamily: 'Futura' }}>Hello and Welcome !</Text>

                                <Text style={{ color: 'red', marginTop: 20, fontSize: 14, textAlign: 'left', fontFamily: 'Futura' }}>Login</Text>


                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    ref={this.flist}
                                    style={{ width: '100%' }}
                                    getItemLayout={this.getItemLayout}
                                    data={this.state.views}
                                    extraData={this.state.views}
                                    renderItem={({ item }) => this.RenderView(item)}
                                />

                            </View>

                        {/* </KeyboardAvoidingView> */}

                    </View>

                    <Toast ref="toast" position='top' />


                </SafeAreaView>
                </KeyboardAwareScrollView>
            )
        }
        else {
            return null;
        }


    }
}