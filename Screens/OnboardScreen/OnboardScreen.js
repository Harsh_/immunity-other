import React, { Component } from 'react';
import {StyleSheet, BackHandler, SafeAreaView, FlatList, Alert, AsyncStorage, ScrollView, Image, View, Text, TextInput, TouchableOpacity, ImageBackground, KeyboardAvoidingView } from 'react-native';
import {styles} from './OnboardStyle.js';
import * as Permissions from 'expo-permissions'
import Constants from 'expo-constants';
import { withNavigation, StackActions, NavigationActions } from 'react-navigation';
import * as Font from 'expo-font';
import Toast, {DURATION} from 'react-native-easy-toast'
import DatePicker from 'react-native-datepicker'




export class OnboardScreen extends Component{

    constructor(props){
        
        super(props);

        this.state ={isLoading:true};
 
        console.log("construct called");        

    }

    componentDidMount(){

        BackHandler.addEventListener('hardwareBackPress', ()=>this.handleBackButton());

    }

    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', ()=>this.handleBackButton());

    }



    handleBackButton = async () => {

        this.props.navigation.goBack();
    } 

    getItemLayout = (data, index) => (
        { length: 50, offset: 50 * index, index }
    )

    
    async componentWillMount() {
        
        
        await Font.loadAsync({
            // Load a font `Montserrat` from a static resource
            
            'Futura': require('../../assets/Fonts/futura.ttf'),
            'Mont': require('../../assets/Fonts/futura.ttf'),
            'Bold': require('../../assets/Fonts/futura.ttf'),
            'Heavy': require('../../assets/Fonts/futura.ttf')
          });

          this.GenerateViews();


    }

    validateEmail = (email) => {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }


    PreSignIn = () =>{

        this.props.navigation.navigate("OTPScreen");
        
    }


    GenerateViews = () =>{

      var viewArr = []

      var view1 = <Text style={{color:'red', marginTop:20, fontSize:14, textAlign:'left', fontFamily:'Futura'}}>What is your full name ?</Text>

      var view2 =  <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}> 
                    <TextInput style={{height: 50, borderRadius:40, minWidth:290, marginTop:20, backgroundColor:'#dadada', textAlign:'center'}}
                        placeholder={"Enter Your Name"}
                        onChangeText={(fullName) => this.setState({fullName})}
                    />
                    <TouchableOpacity style={{marginTop:20}} onPress={()=>this.getDOB()}>
                        <Text style={{color:'black', fontSize:11, textAlign:'center', marginLeft:10, fontFamily:'Futura'}}>OK</Text>
                    </TouchableOpacity>
                    </View>

        

        viewArr.push(view1);
        viewArr.push(view2);

        this.setState({views:viewArr});

        this.setState({isLoading:false});


    }

    getDOB = () =>{

        var viewArr = this.state.views;

        var viewx = <Text style={{color:'red', marginTop:20, fontSize:14, textAlign:'left', fontFamily:'Futura'}}>What is your Date of Birth ? </Text>

        var view5 =  <DatePicker
        style={{width: 200, marginTop:20, borderRadius:25}}
        mode="date"
        format="YYYY-MM-DD"
        minDate="1920-05-01"
        maxDate="2020-11-11"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        date={this.state.date}
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36
          }
        }}
        onDateChange={(date) => {this.SetDate(date)}}
      />

        viewArr.push(viewx)
        viewArr.push(view5);

        this.setState({views:viewArr});

        this.flatListRef.scrollToIndex({animated: true, index: 0});



    }

    SetDate = (date) =>{

        console.log("dates " + date)

        this.setState({date:date})

        var viewArr = this.state.views;

        var viewx = <Text style={{color:'red', marginTop:20, fontSize:14, textAlign:'left', fontFamily:'Futura'}}>You are a : </Text>

        var view5 = <View>
                    <View style={{flexDirection:'row', height:40, borderWidth:1, 
                                    borderRadius:25, borderColor:"black", width:'100%',
                                    justifyContent:'center', alignItems:'center'}}>
                        <TouchableOpacity style={{padding:10}}>
                            <Text>Male</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{padding:10}}>
                            <Text>Female</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{padding:10}}>
                            <Text>Non-Binary</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={{flexDirection:'row', height:40, borderWidth:1, 
                                    borderRadius:25, borderColor:"black", width:'100%',
                                    justifyContent:'center', alignItems:'center', marginTop:10}}>
                            <Text>Do not wish to disclose</Text>
                    </TouchableOpacity>
                    </View> 

        viewArr.push(viewx)
        viewArr.push(view5);

        this.setState({views:viewArr});

        this.flatListRef.scrollToIndex({animated: true, index: 0});


    }

    GoMap = () =>{

        
    }



    RenderView = (item) =>{

        return (
            item
        );
    }



      
    render(){

        if(this.state.isLoading === false){

            return (

              <SafeAreaView style={styles.container}>

                <ScrollView contentContainerStyle={{flexGrow: 1}} showsVerticalScrollIndicator={false}>

                    <View style={styles.sectionTop}>


                    <Image
                            style={{height:55, resizeMode:'contain', marginTop:20}}
                            source={require('../../assets/hlogo.png')}
                        />
                    </View>

                    <KeyboardAvoidingView style={styles.sectionBottom} behavior="padding" keyboardVerticalOffset={40}>

                        <View style={{justifyContent:'flex-end', alignItems:'flex-start', flex:1, marginTop:200}}>

                        <Text style={{color:'red', marginTop:20, fontSize:14, textAlign:'left', fontFamily:'Futura'}}>Please answer the following: </Text>

                        <FlatList
                        showsVerticalScrollIndicator={false}
                        ref={(ref) => { this.flatListRef = ref; }}
                        style={{width:'100%'}}
                        getItemLayout={this.getItemLayout}
                        scrollEnabled={false}
                        data={this.state.views}
                        extraData={this.state.views}
                        renderItem={({item})=>this.RenderView(item)}
                        />

                        </View>

                    </KeyboardAvoidingView>

                    </ScrollView>

                    <Toast ref="toast" position='top'/>

                
                </SafeAreaView>
            )
        }
        else{
            return null;
        }


    }
}