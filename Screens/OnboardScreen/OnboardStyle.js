import { StyleSheet, Platform, StatusBar } from 'react-native'

const styles = StyleSheet.create({

    container:{
        flex:1, backgroundColor:"white", justifyContent:'center',alignItems:'center',
        ...Platform.select({
            ios:{
              marginTop:0,
            },
            android:{
               marginTop: StatusBar.currentHeight,
            }
          }),
    },
    backgroundImage: {
        resizeMode: 'cover',
        width: '100%',
        height:'100%'
    },
    sectionTop:{
        flex:0.1,
        width:'100%',
        justifyContent:'center',
        alignItems:'flex-start'
    },
    sectionBottom:{
        flex:0.9,
        width:'100%',
        justifyContent:'flex-end',
        alignItems:'flex-start',
        marginRight:15
    },
    sectionButton:{
        flex:0.2,
        width:'100%',
        marginTop:25,        
        justifyContent:'flex-start',
        alignItems:'flex-start'
    },
    loginBtn:{

        backgroundColor:"#38b6ff",
        paddingTop:15,
        paddingBottom:15,
        marginBottom:20,
        marginTop:20,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        paddingLeft:20,
        paddingRight:20,
        minWidth:140

    }

});


export { styles }