import React, { Component } from 'react';
import {StyleSheet, SafeAreaView, Image, View, Text, AsyncStorage, StatusBar, Platform} from 'react-native';
import * as Font from 'expo-font';



var styles = StyleSheet.create({

	screen:{
        flex:1,
        backgroundColor:'white',
		justifyContent:'center',
        alignItems:'center',
        ...Platform.select({
            ios:{
              marginTop:0,

            },
            android:{
               marginTop: StatusBar.currentHeight,
            }
          }),
	},
	logo:{

		justifyContent:'center',
		alignItems:'center',
		alignSelf:'center'

	}

});

////////////////////////////////////////////////////////////////////


 export class SplashScreen extends Component{

 	constructor(props){

        super(props);
         
        this.state = {isLoading:true}

    }

     async componentWillMount() {

        await Font.loadAsync({
            // Load a font `Montserrat` from a static resource
            
            'Futura': require('../../assets/Fonts/futura.ttf'),
            'Mont': require('../../assets/Fonts/futura.ttf'),
            'Bold': require('../../assets/Fonts/futura.ttf'),
            'Heavy': require('../../assets/Fonts/futura.ttf')

          });

        this.setState({isLoading:false})


    }

    componentDidMount(){

        setTimeout(() =>{

            this.LoadDetails();
 
        }, 2900);

    }



    LoadDetails = async () => {


        this.props.navigation.navigate("LoginScreen");

    
    }

	render(){

        if(this.state.isLoading === false){

            return (

                <SafeAreaView style={{ flex:1, backgroundColor:'white', justifyContent:'center', alignItems:'center', height:'100%', width:'100%'}}>
    
                <View style={styles.screen}>

                    <View style={{flex:1, justifyContent:'center',alignItems:'center', backgroundColor:"white"}}>

                        <Image
                            style={{height:55, resizeMode:'contain'}}
                            source={require('../../assets/tlogo.png')}
                        />

                        {/* <Text style={{fontFamily:'Bold', fontSize: 29, marginTop:10, textAlign:'center', color:'black'}}>IMMUNITY</Text> */}

                    </View>
                    
                </View>
    
                </SafeAreaView>
    
            );
        }
        else{
            return null;
        }

	}

	

}

////////////////////////////////////////////////////////////////////