import React, { Component } from 'react';
import {StyleSheet, Alert, BackHandler, SafeAreaView, AsyncStorage, ScrollView, Image, View, Text, TextInput, TouchableOpacity, ImageBackground, KeyboardAvoidingView } from 'react-native';
import {styles} from './TermsStyle.js';
import Toast, { DURATION } from "react-native-easy-toast";




export class TermsScreen extends Component{

    constructor(props){
        super(props);

        this.state ={isLoading:true, isImageSelected:false, detailsLoaded:false}
        
        this.LoadDetails();

    }

    componentDidMount() {

        BackHandler.addEventListener('hardwareBackPress', ()=>this.props.navigation.goBack());

    }


    UNSAFE_componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', ()=>this.props.navigation.goBack());

    }

    async UNSAFE_componentWillMount() {
        
        await Expo.Font.loadAsync({
          'Pacifico': require('../../assets/Fonts/futura.ttf'),
          'Sunbird': require('../../assets/Fonts/futura.ttf'),
          'Bull': require('../../assets/Fonts/futura.ttf'),
          'Mont': require('../../assets/Fonts/futura.ttf')

        });

        this.setState({isLoading:false, showDialog:false, 
            uploadDone:false, dialogText:"Sharing Post..",
            uploadError:false});

    }

    LoadDetails = async () => {
        const context = this;
    
        try {
    
          await context.setState({ detailsLoaded: true });
    
        } catch (error) {
          context.setState({ detailsLoaded: true });
        }
    }

    GoOnboard = () =>{

        this.props.navigation.navigate("OnboardScreen")

    }


      
    render(){

        if(this.state.isLoading === false){

            return (

                <SafeAreaView style={{flex: 1, backgroundColor: '#212429'}}>

                <View style={styles.container}>

                    <View style={styles.header}>

                        <Text style={{fontFamily:'Mont', textAlign:'center', alignItems:'center', flex:4, fontSize:20, color:"#ffffff"}}>Terms and Conditions</Text>
                    
                    </View>

					<View style={styles.content} behavior="padding">
                    
                    <ScrollView contentContainerStyle={{flexGrow: 1}}>    

                            <Text style={{fontFamily:'Mont', fontSize:20,  color:'black',
                            textAlign:'center', marginLeft:10,marginRight:10, marginTop:40}}>
                            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...{"\n"}
                            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...{"\n"}
                            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...{"\n"}
                            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...{"\n"}
                            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...{"\n"}
                            </Text>

                            <TouchableOpacity style={styles.loginBtn} onPress={()=>this.GoOnboard()}>
                                <Text style={{color:'white', fontSize:14, textAlign:'center', fontFamily:'Futura'}}>I AGREE</Text>
                            </TouchableOpacity>
                                                
                    </ScrollView>
                    
                    </View>


                    <View>

					</View>

                    <Toast ref="toast" />

                
                </View>
                </SafeAreaView>
            )
        }
        else{
            return null;
        }


    }
}