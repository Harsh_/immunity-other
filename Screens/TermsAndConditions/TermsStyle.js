import { StyleSheet, Platform, StatusBar } from 'react-native'

const styles = StyleSheet.create({

    container:{
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'column',
        flex:1,
		...Platform.select({
            ios:{
              marginTop:0,

            },
            android:{
               marginTop: StatusBar.currentHeight,
            }
          }),
    },
    header:{
        flex:0.1,
        backgroundColor:"#212429",
        width:'100%',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        elevation:5
    },
    content:{
        flex:0.9,
        width:'100%',
        backgroundColor:"#ffffff",
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'
    },
    contentScroll:{
        flex:1,
    },
    imageSelector:{
        flex:3,
        backgroundColor:"#dadada",
        margin:10,
        justifyContent:'center',
        alignItems:'center'
    },
    imageDescriptor:{
        flex:1,
        alignItems:'center'
    },
    loginBtn:{

        backgroundColor:"red",
        paddingTop:15,
        paddingBottom:15,
        marginBottom:20,
        marginTop:20,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        paddingLeft:20,
        paddingRight:20,
        minWidth:140

    }


});


export { styles }